 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.gui;

//import MMM.app;
import Metro.MetroApp;
import Metro.data.DraggableLabel;
import Metro.data.DraggableRectangle;
import Metro.data.Line;
import Metro.data.MetroData;
import static Metro.data.MetroState.ADD_NEW_STATION;
import static Metro.data.MetroState.MOVE_LINE_END;
import static Metro.data.MetroState.MOVE_STATION;
import Metro.data.Station;
import Metro.file.MetroFiles;
import djf.components.AppDataComponent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//Xlint issue
/**
 *
 * @author Colin
 */
public class appController implements Initializable {
    
    @FXML
    private ImageView logoImage;
    @FXML
    private Label label;
    @FXML
    private Button cn_ok;
    @FXML
    private TextField cn_tf;
    @FXML
    private VBox appPane;
    @FXML
    private AnchorPane welcomePane;
    @FXML
    private AnchorPane createNewPane;
    @FXML
    private Button metroStationsBtn;
    @FXML
    private Pane canvas;
    @FXML
    private Slider lineToolbarSlider;
    @FXML
    private Slider stationToolbarSlider;
    @FXML
    private CheckBox toggleGrid;
    
    
    ObservableList<Node> shapes;

    HashMap<String, Station> stationsHash;
    HashMap<String, Line> linesHash;

    MetroApp app;
//    CanvasController canvasController = new CanvasController();
//    MapEditController mapEditController = new MapEditController(app);
    private MetroData mData;
    private ArrayList<Station> stationsList;
    private ArrayList<Line> linesList;
   
    
//    private Stage welcomeStage = (Stage) welcomePane.getScene().getWindow();
//    @FXML
//    private Stage appStage = (Stage) appPane.getScene().getWindow();
//    @FXML
//    private Stage createNewStage = (Stage) createNewPane.getScene().getWindow();
  
    @FXML
    private void exit(ActionEvent event){
        mData.exit();
    }
    //EditToolbar
    @FXML
    private void about(){
        mData.about();  
    }
    //File Toolbar
        @FXML
    private void save() throws IOException{
        mData.save();
    }
    @FXML
    private void saveAs(){
         mData.saveAs();
    }
    @FXML
    private void load(){
        mData.load();
    }
    @FXML
    private void export() throws IOException{
//        MetroFiles mFiles = new MetroFiles();
//        if(mData.getPath() == null){
//            System.out.print("file is null");
//        }
//        else{
//        mFiles.exportData((AppDataComponent)mData, mData.getExportPath());}
    mData.export();
    }
    @FXML
    private void undo(){
        mData.undo();
    }
    @FXML
    private void redo(){
        mData.redo();
    }
    //Metro Lines Toolbar
    
    @FXML
    private void addLine(){
        //((MetroData)app.getDataComponent()).setState(ADD_LINE);
        mData.nameLine();
    }
    @FXML
    private void removeLine(){
        mData.removeSelectedLine();
    }
    @FXML
    private void removeStationFromLine(){
        mData.removeStationFromLine();
    }
    @FXML
    private void editLine(){
        mData.editLine();
    }
    @FXML
    private void addStationToLine(){
        mData.addStationToLine();
    }
    @FXML
    private void adjustLineWidth(){
        mData.setSelectedLineWidth((int) lineToolbarSlider.getValue());
    }
    //Metro Station Toolbar 
    @FXML
    private void editStation(){
        mData.editStation();
        
    }
    @FXML
    private void adjustStationRadius(){
        mData.setSelectedStationRadius((int) stationToolbarSlider.getValue());
    }
    @FXML
    private void openStations(){
        mData.openStations();
    }
  
    @FXML
    private void addNewStation(){
        mData.setState(ADD_NEW_STATION);
    }
    @FXML
    private void removeStation(){
       mData.removeStation();   
    }
    @FXML
    private void openLines(){
       mData.openLines();
    }
    @FXML
    private void listStations(){
        mData.listAllStationsOnLine();
    }
    //Decor Toolbar
    @FXML
    private void removeDecor(){
        if(mData.getSelectedShape() instanceof DraggableLabel){
            DraggableLabel l = (DraggableLabel) mData.getSelectedShape();
            mData.removeShape(mData.getSelectedShape());
//            mData.removeShape(l.getL());
//            mData.removeShape(l);
            
        }else if(mData.getSelectedShape() instanceof Label || mData.getSelectedShape() instanceof DraggableRectangle){
            mData.removeShape(mData.getSelectedShape());
        }    
        
    }
    @FXML
    private void addLabel(){
        mData.addLabel();
    }
    @FXML
    private void setBackgroundColor(){
        mData.setBackgroundColor();        
    }
    @FXML
    private void setImageBackgroud() {
        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        File img = mData.selectImage();
        if(img != null)
            mData.setBackgroundImage(img); 
    }
    @FXML
    private void addImage(){
        File img = mData.selectImage();
        DraggableRectangle d = new DraggableRectangle(new Image(img.getPath()));
        mData.addShape(d);
    }
    //Font toolbar
    @FXML
    private void changeStationText(){
        mData.changeTextColor();
    }
    //canvas functionality
    @FXML
    private void processKey(KeyEvent k){
        //up
                  ///  System.out.print("made it" + k.getText());
        if("w".equals(k.getText())){
           mData.moveShapes(0, -10);

        //down    
        }else if("s".equals(k.getText())){
           mData.moveShapes(0, 10);

        //left
        }else if("a".equals(k.getText())){
           mData.moveShapes(-10, 0);


        }else if("d".equals(k.getText())){
           mData.moveShapes(10, 0);


        }
    }
    @FXML
    private void toggleGrid(){
        if(mData.isGrid()){      
        mData.getShapes().removeIf(p -> p instanceof javafx.scene.shape.Line);
        mData.setIsGrid(false);
        }else{
           mData.initGrid();
           mData.setIsGrid(true);

        }
    }
    @FXML
    private void zoomIn(){
            canvas.setScaleX(canvas.getScaleX() * 1.2);
        canvas.setScaleY(canvas.getScaleY() * 1.2);    
    }
    @FXML
    private void zoomOut(){
    canvas.setScaleX(canvas.getScaleX() / 1.2);
    canvas.setScaleY(canvas.getScaleY() / 1.2);    
    }
    @FXML
    private void increaseMapSize(){
        canvas.setScaleX(canvas.getScaleX() * 1.05);
        canvas.setScaleY(canvas.getScaleY() * 1.05);    
    }
    @FXML
    private void decreaseMapSize(){   
    canvas.setScaleX(canvas.getScaleX() / 1.05);
    canvas.setScaleY(canvas.getScaleY() / 1.05);  
    }
    @FXML
    private void snapToGrid(){
      mData.snapToGrid();
    }
 
   
    @FXML
    private void moveStation(){
        mData.setState(MOVE_STATION);
    }
    @FXML
    private void processCanvasClick(MouseEvent t){ 
        mData.processCanvasClick(t);
    }  
    @FXML
    public void processCanvasMouseRelease(MouseEvent t){
        mData.processCanvasMouseRelease(t);
    }
    @FXML
    private void processCanvasDrag(MouseEvent t){
        mData.processCanvasDrag(t);
    }
    @FXML
    private void moveLineEnd(){
         ((MetroData)app.getDataComponent()).setState(MOVE_LINE_END);
    }
    @FXML
    private void handleCreateNew() throws IOException{
        Scene scene = load("/Metro/Workspace/Workspace.fxml");
        Stage workspace = (Stage) welcomePane.getScene().getWindow();
       workspace.setScene(scene);
    }
    @FXML
    private void moveStationLabel(){
        mData.moveStationLabel();
    }
    @FXML
    private void rotateLabel(){
        mData.rotateLabel();
    }
    @FXML
    private void listLineStations(){
        mData.listAllStationsOnLine();
        
    }
    @FXML
    private void newMap() throws IOException{
           
    }
    private Scene load(String file) throws IOException{
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource(file));
//     
        Scene scene = new Scene(root);
        
        return scene;
    }
    private void Welcome(Stage primaryStage) {
  
    }
    @FXML
    private void initCanvas() {
        mData.setShapes(canvas.getChildren());
    }
    @FXML
    private void findRoute(){
        mData.findRoute();
    }
    @FXML
    private void selectTo(){
        mData.selectTo();
    }
    @FXML
    private void selectFrom(){
        mData.selectFrom();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        app = new MetroApp();
        app.buildAppComponentsHook(canvas);
        stationsHash = new HashMap<String, Station>();
        linesHash = new HashMap<String, Line>();
        linesList = new ArrayList<Line>();
        stationsList = new ArrayList<Station>();
        mData = ((MetroData)app.getDataComponent()) ;
        mData.setHashes(stationsHash, linesHash);
        mData.setStationsArrayList(stationsList);
        mData.setLines(linesList);
    }   
 
    
}
