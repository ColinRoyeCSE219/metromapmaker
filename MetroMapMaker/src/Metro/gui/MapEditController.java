package Metro.gui;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import Metro.data.MetroData;
import Metro.data.MetroState;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppPropertyType.WORK_FILE_EXT;
import static djf.settings.AppPropertyType.WORK_FILE_EXT_DESC;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppMessageDialogSingleton;
//import gol.gui.golWorkspace;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class responds to interactions with other UI logo editing controls.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class MapEditController {
    AppTemplate app;
    MetroData dataManager;
    
    public MapEditController(AppTemplate initApp, MetroData dataManagera) {
	app = initApp;
	this.dataManager = dataManager;
    }
    
    /**
     * This method handles the response for selecting either the
     * selection or removal tool.
     */
    public void processSelectSelectionTool() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.DEFAULT);
//	
//	// CHANGE THE STATE
//	dataManager.setState(golState.SELECTING_SHAPE);	
//	
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to remove the selected shape.
     */
    public void processRemoveSelectedShape() {
//	// REMOVE THE SELECTED SHAPE IF THERE IS ONE
//	dataManager.removeSelectedShape();
//	
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * This method processes a user request to start drawing a rectangle.
     */
    public void processSelectRectangleToDraw() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//	
//	// CHANGE THE STATE
//	dataManager.setState(golState.STARTING_RECTANGLE);
//
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//    
//    /**
//     * This method provides a response to the user requesting to start
//     * drawing an ellipse.
//     */
//    public void processSelectEllipseToDraw() {
//	// CHANGE THE CURSOR
//	Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//	
//	// CHANGE THE STATE
//	dataManager.setState(golState.STARTING_ELLIPSE);
//
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//    
//    public void processAddImage() {
//        Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//        
//
//        
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        
//         
//   
//	 AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//	   // dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
//       
//           
//        // Image img = promptToOpenImage();
//         // if(img != null){
//          //    System.out.print("Success");
//         // }
//		
//	// CHANGE THE STATE
//           dataManager.setState(golState.STARTING_IMAGE);
//        //dataManager.setState(golState.STARTING_RECTANGLE);
//       // dataManager.addShape(image);
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//        
//        
//        /*FileChooser fc = new FileChooser();
//         File file = fc.showOpenDialog(new Stage());
//         if(file!=null){
//             Image img = new Image(file.toURI().toString());*/
         }
    

    void processAddTextButton() {
//        Scene scene = app.getGUI().getPrimaryScene();
//	scene.setCursor(Cursor.CROSSHAIR);
//	
//	// CHANGE THE STATE
//	dataManager.setState(golState.STARTING_TEXT);
//
//	// ENABLE/DISABLE THE PROPER BUTTONS
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method processes a user request to move the selected shape
     * down to the back layer.
     */
    public void processMoveSelectedShapeToBack() {
//	dataManager.moveSelectedShapeToBack();
//	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * This method processes a user request to move the selected shape
     * up to the front layer.
     */
    public void processMoveSelectedShapeToFront() {
//	dataManager.moveSelectedShapeToFront();
//	app.getGUI().updateToolbarControls(false);
    }
        
    /**
     * This method processes a user request to select a fill color for
     * a shape.
     */
    public void processSelectFillColor() {
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	Color selectedColor = workspace.getFillColorPicker().getValue();
//	if (selectedColor != null) {
//	    dataManager.setCurrentFillColor(selectedColor);
//	    app.getGUI().updateToolbarControls(false);
//	}
    }
    
    /**
     * This method processes a user request to select the outline
     * color for a shape.
     */
    public void processSelectOutlineColor() {
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	Color selectedColor = workspace.getOutlineColorPicker().getValue();
//	if (selectedColor != null) {
//	    dataManager.setCurrentOutlineColor(selectedColor);
//	    app.getGUI().updateToolbarControls(false);
//	}    
    }
    
    /**
     * This method processes a user request to select the 
     * background color.
     */
    public void processSelectBackgroundColor() {
//	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
//	Color selectedColor = workspace.getBackgroundColorPicker().getValue();
//	if (selectedColor != null) {
//	    dataManager.setBackgroundColor(selectedColor);
//	    app.getGUI().updateToolbarControls(false);
	}
    
    
    /**
     * This method processes a user request to select the outline
     * thickness for shape drawing.
     */
//    public void processSelectOutlineThickness() {
////	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
////	int outlineThickness = (int)workspace.getOutlineThicknessSlider().getValue();
////	dataManager.setCurrentOutlineThickness(outlineThickness);
////	app.getGUI().updateToolbarControls(false);
//    }
    
    /**
     * This method processes a user request to take a snapshot of the
     * current scene.
     */
//    public void processSnapshot() {
////	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
////	Pane canvas = workspace.getCanvas();
////	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
////	File file = new File("Logo.png");
////	try {
////	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
////	}
////	catch(IOException ioe) {
////	    ioe.printStackTrace();
////	}
//    }


   
//  protected Image promptToOpenImage() {
////	// WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
////	PropertiesManager props = PropertiesManager.getPropertiesManager();
////	
////        // AND NOW ASK THE USER FOR THE FILE TO OPEN
////        FileChooser fc = new FileChooser();   
////        fc.setInitialDirectory(new File(PATH_IMAGES));
////        fc.getExtensionFilters().addAll(new ExtensionFilter("Images","*.png", "*.jpg"));
////	//fc.setTitle(props.getProperty("temp")); // this has something to do with the title of the image
////         File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
////         System.out.print(selectedFile.getName());
////         Image img = new Image(FILE_PROTOCOL + selectedFile.getAbsolutePath());
////         //   fc.getInitialFileName().toString());
////        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
////        //if (selectedFile != null) {
////           // try {
////                // RESET THE WORKSPACE
////		//app.getWorkspaceComponent().resetWorkspace();
////
////                // RESET THE DATA
////               // app.getDataComponent().resetData();
////                
////                // LOAD THE FILE INTO THE DATA
////               // app.getFileComponent().loadData(app.getDataComponent(), selectedFile.getAbsolutePath());
////                
////		// MAKE SURE THE WORKSPACE IS ACTIVATED
////		//app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
////                
////                // AND MAKE SURE THE FILE BUTTONS ARE PROPERLY ENABLED
////                //app.getGUI().updateToolbarControls(saved);
////            //} catch (Exception e) {
////               // AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
////                //dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
////           // }
////      //  }
////        return img;
////        
////        
//
//    }

    void handleCutRequest() {
        dataManager.Cut();
    }

    void handleCopyRequest() throws CloneNotSupportedException {
        dataManager.Copy();
    }

    void handlePasteRequest() {
        dataManager.Paste();
    }
}

