/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.data;

import javafx.scene.control.Label;

/**
 *
 * @author Colin
 */
public class DraggableLabel extends DraggableRectangle {
    Label l;
    public DraggableLabel(String name){
        super();
         l = new Label(name);
         this.setOpacity(.2);
         this.setHeight(15);
         this.setWidth(80);
         this.setVisible(true);
         l.layoutXProperty().bind(this.xProperty());
         l.layoutYProperty().bind(this.yProperty());
    }

    public Label getL(){
        return l;
    }
}
