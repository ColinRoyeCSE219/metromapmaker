package Metro.data;

import Metro.Transactions.AddShape;
import Metro.Transactions.ConnectLine;
import Metro.Transactions.DisconnectLine;
import Metro.Transactions.Drag;
import Metro.Transactions.RemoveShape;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import static Metro.data.MetroState.SELECT;
import Metro.file.MetroFiles;
import Metro.gui.appController;
import djf.components.AppDataComponent;
import djf.AppTemplate;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jtps.jTPS;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import jtps.jTPS_Transaction;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class MetroData implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    
    // THESE ARE THE SHAPES TO DRAW
    private ObservableList<Node> shapes;
    //HASH OF STATIONS AND LINES   
    private HashMap<String, Station> stationsHash;
    private HashMap<String, Line> linesHash;
    //ARRAYLIST OF STATIONS AND LINES
    private ArrayList<Station> stations;
    private ArrayList<Line> linesList;
       
    private Line currentLine;

    private Shape clipboard;
    
    // THIS IS THE CURRENT FILE THAT IS BEING EDITED 
    private File file;
    private String path;
    //REFRENCE TO JTPS OBJECT
    private jTPS undoRedoStack = new jTPS();
    
    // THE BACKGROUND COLOR
    private Color backgroundColor;
    
    // AND NOW THE EDITING DATA
    // THIS IS THE SHAPE CURRENTLY BEING SIZED BUT NOT YET ADDED
    private Shape newShape;

    // THIS IS THE SHAPE CURRENTLY SELECTED
    private Node selectedShape;
    private Node otherSelectedShape;

    // FOR FILL AND OUTLINE
    private Color currentFillColor;
    private Color currentOutlineColor;
    private double currentBorderWidth;

    // CURRENT STATE OF THE APP
    private MetroState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    private AppTemplate app;
    //  THIS IS THE SHARED REFRENCE TO THE CANVAS
    private Pane canvas;
    // USE THIS WHEN THE SHAPE IS SELECTED
    private Effect highlightedEffect;
    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;
    private String exportPath;
    private boolean isGrid = false;
    private ArrayList<Node> grid;
    private String currentFileName;
    private Station fromStation;
    private Station toStation;

    /**
     * This constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public MetroData (AppTemplate initApp) {
	// KEEP THE APP FOR LATER
	app = initApp;
        canvas = app.getCanvas();
        
	// NO SHAPE STARTS OUT AS SELECTED
	newShape = null;
	selectedShape = null;
        
	// INIT THE COLORS
	currentFillColor = Color.web(WHITE_HEX);
	currentOutlineColor = Color.web(BLACK_HEX);
	currentBorderWidth = 1;
	
	// THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.ORANGE);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(7);
	highlightedEffect = dropShadowEffect;
    }
    
    /**
     * This method sets the width of the current line
     * @param scale 
     */
    public void setSelectedLineWidth(double scale){
        if(currentLine != null)
        currentLine.setStrokeWidth(scale*.1); 
    }
    /**
     * This method is used to set the lines and stations hashmaps
     * @param shash
     * @param lhash 
     */
    public void setHashes(HashMap<String, Station> shash ,   HashMap<String, Line> lhash) {
        stationsHash=shash;
        linesHash=lhash;
    }
    /**
     * this  method is used to remove the currently selected shape
     */
    public void removeSelectedShape() {
//        	if (selectedShape != null) {
//
//        jTPS_Transaction transaction = new RemoveSelectedShape(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
//                }
	if (selectedShape != null) {
	    shapes.remove(selectedShape);
	    selectedShape = null;
	}
    }
    /**
    * this method is used to remove a specific shape
    * @param shapeToRemove 
    */
    public void removeShape(Node shapeToRemove) {
        
        jTPS_Transaction transaction = new RemoveShape(shapeToRemove, this);
        undoRedoStack.addTransaction(transaction);
        
    }
    
    
    /**
     * this method is used to remove a station
     */
    public void removeStation() {
       
        removeShape(getSelectedShape()); 
    }
    /**
     * this method is used to remove the selected line
     */
    public void removeSelectedLine() {
//        if (currentLine != null) {
//            shapes.remove(currentLine.getC());
//            shapes.remove(currentLine.getC2());
//            shapes.remove(currentLine.getLabel());
//	    shapes.remove(currentLine);
//	    currentLine = null;
//	}
        jTPS_Transaction transaction = new RemoveShape(currentLine, this);
        undoRedoStack.addTransaction(transaction);
        currentLine = null;

    }
    public void removeStationFromLine(){
        if(selectedShape instanceof DraggableCircle){
        Station s = ((DraggableCircle)selectedShape).getStation();
        jTPS_Transaction transaction = new DisconnectLine(currentLine,s, this);
        undoRedoStack.addTransaction(transaction);
        }

//        if(selectedShape instanceof DraggableCircle){
//            System.out.println("made it yo");
//        currentLine.removeStation(((DraggableCircle)selectedShape).getStation());}
    }
    /**
     * this method is used to move the selected shape to the back of the layout
     */
    public void moveSelectedShapeToBack() {
//	 if (selectedShape != null) {
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
//         }
        
        
        
        if (selectedShape != null) {
	    shapes.remove(selectedShape);
	    if (shapes.isEmpty()) {
		shapes.add(selectedShape);
	    }
	    else {
		ArrayList<Node> temp = new ArrayList<>();
		temp.add(selectedShape);
		for (Node node : shapes)
		    temp.add(node);
		shapes.clear();
		for (Node node : temp)
		    shapes.add(node);
	    }
	}
    }
    /**
     * 
     */
    public void moveStationLabel(){
        if(selectedShape instanceof DraggableCircle){
            DraggableCircle c = (DraggableCircle)selectedShape;
            c.getStation().move(c.getStation().getLabelPosition()+1);
        }
    }
    /**
    * this method is used to send the selected shape to the front
    */
    public void moveSelectedShapeToFront() {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
        
        
	if (selectedShape != null) {
           // undoRedoStack.addTransaction(new MoveShapeToFront(selectedShape));
	   if (selectedShape != null) {
	    shapes.remove(selectedShape);
	    shapes.add(selectedShape);
	}
    }
    }
 
    
    public void selectSizedShape() {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
	if (selectedShape != null)
	    unhighlightShape(selectedShape);
	selectedShape = newShape;
	highlightShape(selectedShape);
	newShape = null;
//	if (state == SIZING_SHAPE) {
//	    state = ((Draggable)selectedShape).getStartingState();
//	}
    }
    /**
     * this method is used to unhighlight a given shape shape
     * @param shape 
     */
    public void unhighlightShape(Node shape) {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
	selectedShape.setEffect(null);
    }
    /**
     * this method is used to highlight a given shape
     * @param shape 
     */
    public void highlightShape(Node shape) {
//	shape.setEffect(highlightedEffect);
//      jTPS_Transaction transaction = new HighlightShape(shape);
//      transaction.doTransaction();
//      undoRedoStack.addTransaction(transaction);
              shape.setEffect(highlightedEffect);
    }
    
    /*
    public void startNewRectangle(int x, int y) {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
	DraggableRectangle newRectangle = new DraggableRectangle();
	newRectangle.start(x, y);
	newShape = newRectangle;
	//ainitNewShape();
    }
*/
    /**
     * this method adds a given shape to the observable list.
     * @param shapeToAdd 
     */
    public void addShape(Node shapeToAdd) {
        
        jTPS_Transaction transaction = new AddShape(shapeToAdd, this);
        //transaction.doTransaction();
        undoRedoStack.addTransaction(transaction);
    }
    /**
     * this method returns the current state of the program
     * @param testState
     * @return 
     */
    public boolean isInState(MetroState testState) {   
	return state == testState;
    }
    /**
     * cut is a pretty straight forward opperation
     */
    public void Cut() {
        try {
            Copy();
        } catch (CloneNotSupportedException ex) {
            
        }
        removeSelectedShape();
    }
    /**
     * so is copy
     * @throws CloneNotSupportedException 
     */
    public void Copy() throws CloneNotSupportedException {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
        
        
        
        
//        if(getSelectedShape() instanceof DraggableRectangle){
//            DraggableRectangle shape = (DraggableRectangle) getSelectedShape();
//            if(shape.getIsImage()){
//                clipboard = shape.clone(shape.getImage());
//            }else if(shape.getIsText()){
//                clipboard = shape.clone(shape.getText());
//            }else{
//                clipboard = shape.clone();
//            }
//        }
//        
//        if(getSelectedShape() instanceof DraggableEllipse){
//            DraggableEllipse shape = (DraggableEllipse) getSelectedShape() ;
//            clipboard =  shape.clone();
//            }   
//        
    }
    /**
     * not writing this
     */
    public void Paste() {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
        
        
        
        
//         Shape shape = clipboard;
//         if(shape instanceof DraggableRectangle){
//             if(((DraggableRectangle) shape).isImage){
//                 
//             }else if(((DraggableRectangle) shape).isText){
//                 
//             }else{
//                 
//             }
//         }
//        if(getSelectedShape() instanceof DraggableEllipse){
//                state = MetroState.STARTING_ELLIPSE;
//
//                shape = getSelectedShape();
//                 DraggableEllipse temp = (DraggableEllipse) shape;
//                 temp = temp.clone();
//                 temp.start((int) temp.getX(), (int) temp.getY());
//                 newShape = temp;
//                 initNewShape();
//                }
//            }    
    }
    
   /**
    * this method displays some information about the application and it's developer
    */
    public void about() {
         Text text = new Text("Metro Map Maker   jtps,djf,propertiesmanager, Colin Roye debugging enterprise");
        FlowPane fp_about = new FlowPane(text);
        Scene aboutscene = new Scene(fp_about);
        Stage aboutStage = new Stage();
        aboutStage.setScene(aboutscene);
        aboutStage.show();
    }
    /**
     * thins method lists all the stations on a line in a new pane
     */
    public void listAllStationsOnLine(){
        VBox v = new VBox();
        Scene scene = new Scene(v);
        Stage stage = new Stage();
       
        if(currentLine != null){
            for(Station s: currentLine.getStations()){
                Button btn = new Button(s.getName());
                btn.setOnAction(e ->{
                    stage.close();
                });
                v.getChildren().add(btn);
                
            }
            stage.setScene(scene);
            stage.show();
        }
    }
    public void selectFrom(){
        
                VBox v = new VBox();
        Scene scene = new Scene(v);
        Stage stage = new Stage();
       
        
            for(Node shape: shapes){
                if(shape instanceof DraggableCircle){
                    DraggableCircle c = (DraggableCircle) shape;
                    if(!c.getIsEnd()){
                        
                Button btn = new Button(c.getStation().getName());
                btn.setOnAction(e ->{
                    fromStation = c.getStation();
                    stage.close();
                });
                v.getChildren().add(btn);
                }
              }
            }
            stage.setScene(scene);
            stage.show();
        }
       
    
    public void selectTo(){
            
                VBox v = new VBox();
        Scene scene = new Scene(v);
        Stage stage = new Stage();
       
        
            for(Node shape: shapes){
                if(shape instanceof DraggableCircle){
                    DraggableCircle c = (DraggableCircle) shape;
                    if(!c.getIsEnd()){
                        
                Button btn = new Button(c.getStation().getName());
                btn.setOnAction(e ->{
                    toStation = c.getStation();
                    stage.close();
                });
                v.getChildren().add(btn);
                }
              }
            }
            stage.setScene(scene);
            stage.show();
    }
    public void findRoute(){
        LinkedList<Station> path = new LinkedList<Station>();
        if(fromStation != null && toStation != null){
           path = bfs(fromStation, toStation);
        }
                Text tf = new Text();

        for(Station s: path){
            //System.out.println(s.getName());
            tf.setText(tf.getText()+" "+s.getName());
        }
        Stage stage = new Stage();
        HBox h = new HBox();
        h.getChildren().add(tf);
        Button ok = new Button("ok");
         h.getChildren().add(ok);
         stage.setScene(new Scene(h));
         stage.show();
    }
    public LinkedList<Station> bfs(Station from, Station to){
		
		//mark first actor as true
		//add to que
		//while que is not empty deque first element and add it to linked list
		//take friends of last dequed and add to que
		//if person has not been visted add to que
		LinkedList<Station> queue = new LinkedList<Station>();
		LinkedList<Station> list = new LinkedList<Station>();

		//add first actor to ques
		
		/*String[] temp = actors.split(", ");
		String firstActor = temp[0];
		String secondActor = temp[1];*/
//                //if not neighbor
//		if(!from.getNextTo().contains(to)){
//			queue.add(from);
//			Station currentActor = from;
//			currentActor.setVisited(true);
//		while(queue.size() > 0/*que is not empty*/){
//			currentActor = queue.remove();
//			list.add(currentActor);
//		for(Station i: currentActor.getNextTo()/*all friends*/){
//			if(!i.getVisited()/*not visited*/){
//			//add to que 
//			//mark as visted
//				queue.add(i);
//				i.setVisited(true);
//				i.addToPath(currentActor.getPath(), currentActor);
//				
//				}	
//			}
//		}
//		
//		}
                from.setVisited(true);
                int counter = 1;
                for(Station s: from.getNextTo()){
                   
                    if(s.getVisited()){
                        counter++;
                    }
                    else{
                    if(s.getName().equals(to.getName())){
                        list.add(s);
                        return list;
                    }
                    else{
                        list.addAll(bfs(s, to));
                        list.add(s);
                    }
                
                	

    }
    }
                return null;
    }
    
    /**
     * this method opens a dialog to select a station
     */
    public void openStations() {
            Stage stage = new Stage();
        
        VBox v = new VBox();
        v.setPrefSize(150, 40*(currentLine.getStations().size()-1));
        Scene scene = new Scene(v);
        
        for(Station s : currentLine.getStations()){
            Button temp = new Button(s.getName());
            temp.setPrefWidth(20);
           temp.setPrefWidth(200);
            v.getChildren().add(temp);
            temp.setOnMouseClicked(e ->{
                setOtherShape(s.getCircle());
                stage.close();
            });
            stage.setScene(scene);
            stage.show();
        }
    }
    /**
     * this method opens a dialog to select a line
     */
    public void openLines() {
        Stage stage = new Stage();
        
        VBox v = new VBox();
        v.setPrefSize(150, 40*(linesList.size()-1));
        Scene scene = new Scene(v);
        
        for(Line s : linesList){
            Button temp = new Button(s.getName());
            temp.setPrefWidth(20);
            temp.setPrefWidth(200);
            v.getChildren().add(temp);
            temp.setOnMouseClicked(e ->{
                setCurrentLine(s);
                System.out.print("current Line :" + s.getName());
                stage.close();
            });
            stage.setScene(scene);
            stage.show();
        }
    }
    /**
     * this method is used to save the application
     */
    public void save() {
            // System.out.print(file.getAbsolutePath());
         if(currentFileName == null){
             saveAs();
         }else{
            setFile(new File("./work/" + currentFileName));
            MetroFiles mFiles = new MetroFiles();
        if(getPath() == null){
            System.out.print("file is null");
        }
              else{
               try {
               mFiles.saveData(this, getPath());
             } catch (IOException ex) {
               Logger.getLogger(appController.class.getName()).log(Level.SEVERE, null, ex);
                 }
}
    }
    }
    public void saveAs(){

        Stage stage = new Stage();
        HBox h = new HBox();
        TextField tf = new TextField();
        Button ok = new Button("ok");
         h.getChildren().addAll(tf, ok);
         stage.setScene(new Scene(h));

        ok.setOnMouseClicked(e->{
                System.out.print(tf.getText());
                 currentFileName = tf.getText();

        setFile(new File("./work/" + currentFileName));
            MetroFiles mFiles = new MetroFiles();
        if(getPath() == null){
            System.out.print("file is null");
            
        }
        else{
               try {
               mFiles.saveData(this, getPath());
             } catch (IOException ex) {
               Logger.getLogger(appController.class.getName()).log(Level.SEVERE, null, ex);
                 }
}
         stage.close();
        });
          stage.show();
}

    public void export(){
        Stage stage = new Stage();
        HBox h = new HBox();
        TextField tf = new TextField();
        Button ok = new Button("ok");
        h.getChildren().addAll(tf, ok);
        stage.setScene(new Scene(h));

        ok.setOnMouseClicked(e->{
                System.out.print(tf.getText());
                 String name = tf.getText();

         setExportFile(new File("./export/" + name));
            MetroFiles mFiles = new MetroFiles();
        if(getExportPath() == null){
            System.out.print("file is null");
            
        }
        else{
               try {
               mFiles.exportData(this, getExportPath());
               WritableImage i = new WritableImage((int)canvas.getWidth(),(int)canvas.getHeight());
               SnapshotParameters params = new SnapshotParameters();
              
           
               i = canvas.snapshot(params, i);
               
               
               BufferedImage bi = SwingFXUtils.fromFXImage(i, null);
               File outputfile = new File("./export/"+name+"exp.png");
               ImageIO.write(bi, "png", outputfile);
               ///canvas.snapshot(params, image);
             } catch (IOException ex) {
               Logger.getLogger(appController.class.getName()).log(Level.SEVERE, null, ex);
                 }
}
         stage.close();
        });
          stage.show();
    }
    
    /**
     * this method is used to load a file
     */
    public void load() {
         File[] files = new File(PATH_WORK).listFiles();
        initRecentFileButtons(files);
    }
    /**
     * this method is used to exit the application
     */
    public void exit() {
        //may need to ask if app wants to be saved
                System.exit(0);
    }

    /**
     * this method is used to list all the recent files in the directory
     * @param files 
     */
    private void initRecentFileButtons(File[] files) {
        int index = 0;
        VBox vbox = new VBox();
        Scene scene = new Scene(vbox);
        Stage stage = new Stage();
        
        for (File file : files) {
        if (file.isDirectory()) {
            initRecentFileButtons(file.listFiles()); // Calls same method again.
        } else {
            Button btn = new Button(file.getName());
            btn.setPrefSize(200, 200);
            vbox.getChildren().add(btn);
            
            btn.setOnAction((e) -> {
                MetroFiles f = new MetroFiles();
                try {
                    f.loadData(this, "./work/" + file.getName());
                } catch (IOException ex) {
                    Logger.getLogger(appController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            });
            System.out.println("File: " + file.getName());
        }
            }  
        stage.setScene(scene);
        stage.show();
        }
    /**
     * this method processes the mouse click action
     * @param t 
     */
    public void processCanvasClick(MouseEvent t) {
              MetroData data = (MetroData)app.getDataComponent();
        
        int x = ((int)t.getX());
        int y = ((int)t.getY());
        
         if (data.isInState(SELECT)) {
             
            // SELECT THE TOP SHAPE
            
            Node shape = data.selectTopShape(x, y);
           data.setSelectedShape(shape);
           
            // AND START DRAGGING IT
            if (shape != null) {
                
                if(shape instanceof DraggableCircle){
                    DraggableCircle c = (DraggableCircle)shape;
                    c.start((int)c.getX(), (int)c.getY());
            }
         }
        }else if(data.isInState(MetroState.ADD_NEW_STATION)){  
           System.out.print("made it");
           nameStation(x, y);
           data.setState(SELECT);}
            
        
    }
    /**
     * this method processes the mouse the release of the mouse
     * @param t 
     */
    public void processCanvasMouseRelease(MouseEvent t) {
        MetroData data = (MetroData)app.getDataComponent();
        data.setShapes(canvas.getChildren());
        
        int x = ((int)t.getX());
        int y = ((int)t.getY());
        if(selectedShape!= null){
        jTPS_Transaction transaction = new Drag(selectedShape,this,x,y);
        undoRedoStack.addTransaction(transaction);}
            //data.setState(SELECT);
    }
    /**
     * this method processes the drag of the mouse
     * @param t 
     */
    public void processCanvasDrag(MouseEvent t) {
        int x = ((int)t.getX());
        int y = ((int)t.getY());
       
       // if (isInState(MetroState.DRAGGING_STATION)) {
            if(selectedShape instanceof DraggableCircle){
            Draggable selectedDraggableShape = (Draggable) selectedShape;
            selectedDraggableShape.drag(x, y);
           // }else 
            }
            if(selectedShape instanceof DraggableRectangle){
               Draggable selectedDraggableShape = (Draggable) selectedShape;
                selectedDraggableShape.drag(x, y);
             }
    }
       
    /**
     * opens a dialog to make a new station
     * @param x
     * @param y 
     */
    public void nameStation(int x, int y) {
                Stage stage = new Stage();
        HBox h = new HBox();
        TextField tf = new TextField();
        Button ok = new Button("ok");
         h.getChildren().addAll(tf, ok);
         stage.setScene(new Scene(h));

        ok.setOnMouseClicked(e->{
            String name = tf.getText();
            if(verifyNameStation(name)){
           Station s = new Station(x,y,name);
           jTPS_Transaction transaction = new AddShape(s,name,this);
          // transaction.doTransaction();
           undoRedoStack.addTransaction(transaction);
          // DraggableCircle c = s.getCircle();
           
//           Label l = s.getLabel();
//           //((MetroData) app.getDataComponent()).addShape(p);
//
//           ((MetroData) app.getDataComponent()).addShape(c);
//           ((MetroData) app.getDataComponent()).addShape(l);
//           stationsHash.put(name, s);
//           stations.add(s);
           stage.close();
            }
        });
            
                    stage.show();

    }
    /**
     * this method is used to name a line
     */
    public void nameLine() {
        Stage stage = new Stage();
        HBox h = new HBox();
        TextField tf = new TextField();
        Button ok = new Button("ok");
         h.getChildren().addAll(tf, ok);
         stage.setScene(new Scene(h));

        ok.setOnMouseClicked(e->{
         String name = tf.getText();
        if(verifyNameLine(name)){
         Line line = new Line(name);
         
           jTPS_Transaction transaction = new AddShape(line,name,this);
          // transaction.doTransaction();
           undoRedoStack.addTransaction(transaction);
//            addShape(line);
//            addShape(line.getC());
//            addShape(line.getC2());
//            addShape(line.getLabel());
//
//           linesHash.put(name, line);
//           linesList.add(line);
           stage.close();
            }
        });
          stage.show();    
    }
    /**
     * verifies if name has been taken
     * @param name
     * @return 
     */
    private boolean verifyNameStation(String name) {
        if(stationsHash.get(name) == null && !name.equals("")){
            return true;
        }
            return false;
//        if((Station)(stationsHash.get(name))){
//            System.out.print("made itddd");
//            return true;
//        }
//        return false;
    }
    /**
     * verifies if name has been taken
     * @param name
     * @return 
     */
    private boolean verifyNameLine(String name) {
        if(linesHash.get(name) == null && !name.equals("")){
            return true;
        }
            return false;
//        if((Station)(stationsHash.get(name))){
//            System.out.print("made itddd");
//            return true;
//        }
//        return false;
    }
    /**
     * add station to line
     */
    public void addStationToLine() {
          Line l =  getCurrentLine();

    if( getSelectedShape() instanceof  DraggableCircle && getCurrentLine() != null){
            jTPS_Transaction transaction = new ConnectLine(l,((DraggableCircle)(getSelectedShape())).getStation(),this);
            undoRedoStack.addTransaction(transaction);
    }
    } 
    /**
     * adds line to station
     */
    public void addLine() {
        if(selectedShape != null && otherSelectedShape != null && selectedShape != otherSelectedShape &&  otherSelectedShape instanceof DraggableCircle && selectedShape instanceof DraggableCircle){
                System.out.println(((DraggableCircle)selectedShape).getStation().getName());
                System.out.println(((DraggableCircle)otherSelectedShape).getStation().getName());

              ((DraggableCircle)selectedShape).getStation().getCircle().centerXProperty().bind(((DraggableCircle)otherSelectedShape).centerXProperty());
              ((DraggableCircle)selectedShape).getStation().getCircle().centerYProperty().bind(((DraggableCircle)otherSelectedShape).centerYProperty());

        }
    }

    public ObservableList<Node> getShapes() {
        return shapes; //nodes from the 
    }
    public Node getTopShape(int x, int y) {
	for (int i = shapes.size() - 1; i >= 0; i--) {
	    Node shape = (Node)shapes.get(i);
	    if (shape.contains(x, y)) {
		return shape;
	    }
	}
        return null;
    }
    public Node getNewShape() {
        return newShape;
    }
    

    public Node getSelectedShape() {
        return selectedShape;
    }
     public MetroState getState() {
        return state;
    }
    public Line getCurrentLine() {
       return currentLine;
    }
    public String getPath(){
        return path;
    }
    public String getExportPath(){
        return exportPath;
    }
     public ArrayList<Station> getStations(){
        return stations;
    }
    public ArrayList<Line> getLines(){
        return linesList;
    }
    public void setShapes(ObservableList<Node> initShapes) {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
        
        
	shapes = initShapes;
    }  
    /**
     * this method is used to set an image as the background of the workspace;
     * @param img 
     */
    public void setBackgroundImage(File img) {
//        Image image = new Image(img.getPath());
//        //ImageView iv = new ImageView(img);
//        String image = JavaFXApplication9.class.getResource(PATH_IMAGES + "img.getName()").toExternalForm();
//           canvas.setStyle("-fx-background-image: url(" + PATH_IMAGES + img.getName() + ");");
Image image = new Image(img.getPath());
// new BackgroundSize(width, height, widthAsPercentage, heightAsPercentage, contain, cover)
BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
// new BackgroundImage(image, repeatX, repeatY, position, size)
BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
// new Background(images...)
Background background = new Background(backgroundImage);
canvas.setBackground(background);
    }
    /**
     * this method opens a color picker which allows the user to select a color  to use for the background
     */
    public void setBackgroundColor() {
        ///
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
          ColorPicker backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
          VBox v = new VBox();
          Button btnOk = new Button("ok");
          v.getChildren().add(backgroundColorPicker);
          v.getChildren().add(btnOk);
          Scene scene = new Scene(v);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          btnOk.setOnAction(e->{
            String color = backgroundColorPicker.getValue().toString();
            color = color.substring(2, color.length()-2);
            System.out.print(color);
            canvas.setStyle("-fx-background-color: #"+ color +";");
           });
//        VBox v = new VBox();
//        Scene scene = new Scene(v);
//        Stage stage = new Stage();
//        stage.setScene(scene);
//        
//        Button blue = new Button("Blue");
//        v.getChildren().add(blue);
//        Button red = new Button("Red");
//        v.getChildren().add(red);
//        Button orange = new Button("Orange");
//        v.getChildren().add(orange);
//        Button black = new Button("Black");
//        v.getChildren().add(black);
//
//         black.setOnAction(e->{
//            String color = (Color.BLACK).toString();
//            color = color.substring(3, color.length()-1);
//            canvas.setStyle("-fx-background-color: #"+ color +";");
//         });
//         blue.setOnAction(e->{
//            String color = (Color.BLUE).toString();
//            color = color.substring(3, color.length()-1);
//            canvas.setStyle("-fx-background-color: #"+ color +";");
//        });
//          orange.setOnAction(e->{
//            String color = (Color.ORANGE).toString();
//            color = color.substring(3, color.length()-1);
//            canvas.setStyle("-fx-background-color: #"+ color +";");
//        });
//         red.setOnAction(e->{
//            String color = (Color.RED).toString();
//            color = color.substring(3, color.length()-1);
//            canvas.setStyle("-fx-background-color: #"+ color +";");
//        });
//        stage.show();
//	
	//canvas.setBackground(background);
    }
    public HashMap<String, Station> getStationsHash() {
        return stationsHash;
    }
    public HashMap<String, Line> getLinesHash() {
        return linesHash;
        
    }
    

    public void setCurrentOutlineColor(Color initColor) {
        ///
        
//        jTPS_Transaction transaction = new MoveShapetoTheBack(selectedShape);
//        transaction.doTransaction();
//        undoRedoStack.addTransaction(transaction);
        
	currentOutlineColor = initColor;
	if (selectedShape != null) {
	    //selectedShape.setStroke(initColor);
	}
    }
    public Node selectTopShape(int x, int y) {
     
	Node shape = getTopShape(x, y);
	if (shape == selectedShape)
	    return shape;
	
	if (selectedShape != null) {
	    unhighlightShape(selectedShape);
	}
	if (shape != null) {
	    highlightShape(shape);
	}
	selectedShape = shape;
	
	return shape;
    }
    public void setStationsArrayList(ArrayList<Station> stationsList) {
        stations = stationsList;
    }
    public void setLines(ArrayList<Line> init) {
        linesList = init;
    }
    public void setFile(File initFile){
        file = initFile;
        path = initFile.getPath();
    }
     public void setExportFile(File initFile){
        file = initFile;
        exportPath = initFile.getPath();
    }
    public void setCurrentLine(Line s) {
         currentLine = s;
    }
    public void setState(MetroState initState) {
	state = initState;
    }
    public void setSelectedShape(Node initSelectedShape) {
   	selectedShape = initSelectedShape;
        System.out.print("asdf");
    }
    public void setOtherShape(Node shape) { 
        otherSelectedShape = shape;
    } 

    public void setSelectedStationRadius(double scale) {
        if(selectedShape instanceof DraggableCircle){
        ((DraggableCircle)selectedShape).setRadius(scale);
        }
        
    }
    public void undo(){
        undoRedoStack.undoTransaction();
    }
    public void redo(){
        undoRedoStack.doTransaction();
    }

    public void editLine() {
          ColorPicker backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
          VBox v = new VBox();
          Button btnOk = new Button("ok");
          v.getChildren().add(backgroundColorPicker);
          v.getChildren().add(btnOk);
          Scene scene = new Scene(v);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          btnOk.setOnAction(e->{
            if(currentLine != null){
            String color = backgroundColorPicker.getValue().toString();
            color = color.substring(2, color.length()-2);
            System.out.print("#" + color);
            currentLine.setStroke(Color.web("#"+ color));
            currentLine.getC().setFill(Color.web("#"+ color));
            currentLine.getC2().setFill(Color.web("#"+ color));
            }
           });
        stage.show();
        
        
    }
        public void editStation() {
          ColorPicker backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
          VBox v = new VBox();
          Button btnOk = new Button("ok");
          v.getChildren().add(backgroundColorPicker);
          v.getChildren().add(btnOk);
          Scene scene = new Scene(v);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          btnOk.setOnAction(e->{
            if(selectedShape != null && selectedShape instanceof DraggableCircle){

            String color = backgroundColorPicker.getValue().toString();
            color = color.substring(2, color.length()-2);
            System.out.print("#" + color);
            ((DraggableCircle)selectedShape).setFill(Color.web("#"+ color));
            }
           });
      
        stage.show();
    }
       public File selectImage() {
        FileChooser fc = new FileChooser();   
        fc.setInitialDirectory(new File(PATH_IMAGES));
        System.out.print(PATH_IMAGES);
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Images","*.png", "*.jpg"));
          //fc.setInitialDirectory( new File(System.getProperty("user.home"))); 
	//fc.setTitle(props.getProperty("temp")); // this has something to do with the title of the image
        Stage stage = new Stage();
        File selectedFile = fc.showOpenDialog(stage);
        File img = null;
        if(selectedFile != null){
        System.out.print(selectedFile.getName());
        img = new File(FILE_PROTOCOL + selectedFile.getAbsolutePath());
        }
        return img;
    }
  
    public void addLabel() {
        Stage stage = new Stage();
        HBox h = new HBox();
        TextField tf = new TextField();
        Button ok = new Button("ok");
         h.getChildren().addAll(tf, ok);
         stage.setScene(new Scene(h));

        ok.setOnMouseClicked(e->{
            
        // transaction.doTransaction();
        
        String name = tf.getText();
        DraggableLabel l = new DraggableLabel(name);
        jTPS_Transaction transaction = new AddShape(l,this);
        undoRedoStack.addTransaction(transaction);

      
        });
          stage.show();    
    }

    public void rotateLabel() {
        if(selectedShape instanceof DraggableCircle){
            DraggableCircle c = (DraggableCircle)selectedShape;
            if(!c.getIsEnd()){
                c.getStation().rotate();
            }
        }
    }

    public void changeTextColor() {
        //can be draggable rectangle(label, draggable circle(end of line/ station)
        ColorPicker backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
          VBox v = new VBox();
          Button btnOk = new Button("ok");
          v.getChildren().add(backgroundColorPicker);
          v.getChildren().add(btnOk);
          Scene scene = new Scene(v);
          Stage stage = new Stage();
          stage.setScene(scene);
          stage.show();
          btnOk.setOnAction(e->{
           
            String color = backgroundColorPicker.getValue().toString();
            color = color.substring(2, color.length()-2);
             if(selectedShape == null){
                if(currentLine != null){
                    currentLine.getLabel().setTextFill(Color.web(color));
                }
            }else{
            if(selectedShape instanceof DraggableLabel){
                DraggableLabel l = (DraggableLabel)selectedShape;
                l.getL().setTextFill(Color.web(color));
            }
            if(selectedShape instanceof DraggableCircle){
            DraggableCircle c = (DraggableCircle)selectedShape;
            if(!c.getIsEnd()){
                c.getStation().getLabel().setTextFill(Color.web(color));
                    }
                   }
                 }
             stage.close();
           });
        stage.show();
       
    }


    public void moveShapes(int x, int y){
          for(Node shape: getShapes()){
               if(shape instanceof Draggable){
                   Draggable d = (Draggable)shape;
                   d.drag((int)d.getX()+x,(int) d.getY()+y);
               }
           }
    }

    public boolean isGrid() {
        
        initGrid();
        return isGrid;
    }

    public void drawGrid() {
        
    }

    public void unDrawGrid() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void initGrid() {
        grid = new ArrayList<Node>();
        //create the x
//        while(){
//        javafx.scene.shape.Line line = new javafx.scene.shape.Line();
//       
//        }
//        //create the y
//        while(){
//           
//        }
        for (int i = 0; i <1500; i+=50)
{
    javafx.scene.shape.Line line1 = new javafx.scene.shape.Line(i, 0, i, canvas.getHeight());
    line1.setStroke(Color.LIGHTGRAY);
    javafx.scene.shape.Line line2 = new javafx.scene.shape.Line(0, i, canvas.getWidth(), i);
    line2.setStroke(Color.LIGHTGRAY);
    shapes.add(0, line2);
    shapes.add(0,line1);
}
    }

    public void setIsGrid(boolean b) {
        isGrid=b;
    }

    public void snapToGrid() {
        if(selectedShape instanceof DraggableCircle){
            DraggableCircle c = (DraggableCircle)selectedShape;
            int x = (int)c.getX();
            int y = (int)c.getY();
            if(x%50<=25){
                 x = (int)c.getX();
                 y = (int)c.getY();
              //  c.drag((int)(c.getX()-(x%50)), y);
                c.setX((int)(c.getX()-(x%50)));
            }
            else{
                 x = (int)c.getX();
                 y = (int)c.getY();
               // c.drag((int)c.getX()+(50 - x%50),y);
                c.setX((int)c.getX()+(50 - x%50));
            }
            
            if(y%50<=25){
                 x = (int)c.getX();
                 y = (int)c.getY();
                c.drag(x, (int)(c.getY()-(y%50)));
                c.setY((int)(c.getY()-(y%50)));
            }else{
                  x = (int)c.getX();
             y = (int)c.getY();
                c.drag(x, (int)(c.getY()+(50 - y%50)));
                c.setY((int)(c.getY()+(50 - y%50)));
            }
        }
    }
    


    


}