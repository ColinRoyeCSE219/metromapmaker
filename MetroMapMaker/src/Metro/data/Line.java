/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.data;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Colin
 */
public class Line extends Path{
    private String name;
    private boolean isCircular;
    private Color color;
    private DraggableCircle c;
    private DraggableCircle c2; 
    private LineTo line;
    private MoveTo begin;
    private Label l;
    private ArrayList<Station> stations;
    private JsonArray stationsJson;
    public Line(String name){
            line = new LineTo();
            begin = new MoveTo();
            begin.setX(500);
            begin.setY(100);
            line.setX(100);
            line.setY(100);
            stations = new ArrayList<Station>(); 
            c = new DraggableCircle(((int)line.getX()), ((int)line.getY()), true);
            c2 = new DraggableCircle(((int)begin.getX()), ((int)begin.getY()), true);
            this.name = name;
            l = new Label(name);
            l.layoutXProperty().bind(c.centerXProperty());
            l.layoutYProperty().bind(c.centerYProperty());
            
            line.xProperty().bind(c.centerXProperty());
            line.yProperty().bind(c.centerYProperty());

            begin.xProperty().bind(c2.centerXProperty());
            begin.yProperty().bind(c2.centerYProperty());
            
            getElements().addAll(begin, line);
            
    }
       public Line(String name, MoveTo mIn, LineTo lIn, JsonArray initStationsJson){
            line = new LineTo();
            begin = new MoveTo();
            begin.setX(mIn.getX());
            begin.setY(mIn.getY());
            line.setX(lIn.getX());
            line.setY(lIn.getY());
            this.stationsJson = initStationsJson;
           // stations = stationsIn; 
            c = new DraggableCircle(((int)line.getX()), ((int)line.getY()), true);
            c2 = new DraggableCircle(((int)begin.getX()), ((int)begin.getY()), true);
            this.name = name;
            l = new Label(name);
            l.layoutXProperty().bind(c.centerXProperty());
            l.layoutYProperty().bind(c.centerYProperty());
            
            line.xProperty().bind(c.centerXProperty());
            line.yProperty().bind(c.centerYProperty());

            begin.xProperty().bind(c2.centerXProperty());
            begin.yProperty().bind(c2.centerYProperty());
            
            getElements().addAll(begin, line);
            
    }
    public JsonArray getStationsJson(){
        return stationsJson;
    }
    public Line() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return the stations
     */
    public ArrayList<Station> getStations() {
        return stations;
    }

    /**
     * @param stations the stations to set
     */
    public void add(Station station) {
        getStations().add(station);
    }

    /**
     * @return the c
     */
    public DraggableCircle getC() {
        return c;
    }

    /**
     * @return the c2
     */
    public DraggableCircle getC2() {
        return c2;
    }

    /**
     * @return the line
     */
    public LineTo getLine() {
        return line;
    }

    /**
     * @return the begin
     */
    public MoveTo getBegin() {
        return begin;
    }
    public Label getLabel(){
        return l;
    }
    public void sortStations(){
        for(int i = 0; i < stations.size(); i++){
            for(int j = 1; j < stations.size(); j++){
                if(getDistance(stations.get(j-1)) > getDistance(stations.get(j))){
                    Station temp = stations.get(j-1);
                    stations.set(j-1, stations.get(j));
                    stations.set(j,temp);
                }    
            }
        }
    }
    public void addStation(Station s){
        stations.add(s);
        sortStations();
        //get the one before s, get the one after s and connect
        // if the one before s  is less then 0 then it is connected to c
        // if the one before s is greater thatn arraylist.size() conncet to c2
        assembleLine();
     
        
    }

    public void assembleLine() {
        
        for(Station s: stations){
        if ((stations.indexOf(s) - 1) < 0){
            //conncet to c
            connect(s, c);
           // connect(s, stations.get(stations.indexOf(s)+1));

        }
        else if ((stations.indexOf(s) + 1) >= stations.size()){
            //conncet to c
            connect(s, c2);
            //connect(s, stations.get(stations.indexOf(s)-1));

        }
        else{
            connect(s);

        }
       }
    }

    public int getDistance(Station s) {
        int x1 = ((int)s.getCircle().getX());
        int x2 = ((int)begin.getX());
        int y1 = ((int)s.getCircle().getY());
        int y2 = ((int)begin.getY());
        int xf = (int)Math.pow(x1 - x2, 2);
        int yf = (int)Math.pow(y1 - y2, 2);
        int dist = (int) Math.sqrt(xf+yf);
        return dist;
    }

    private void connect(Station s, DraggableCircle c) {
        int i = (stations.indexOf(s)+1);
        LineTo nLine = new LineTo();
        

        nLine.setX(s.getCircle().getX());
        nLine.setY(s.getCircle().getY());
       nLine.yProperty().bind(s.getCircle().centerYProperty());
       nLine.xProperty().bind(s.getCircle().centerXProperty());
        getElements().add(i, nLine);
        
    }
    private void connect(Station s) {
       
        int i = (stations.indexOf(s)+1);
        LineTo nLine = new LineTo();
        nLine.setX(s.getCircle().getX());
        nLine.setY(s.getCircle().getY());
        
        getElements().remove(i);
        
        nLine.yProperty().bind(s.getCircle().centerYProperty());
        nLine.xProperty().bind(s.getCircle().centerXProperty());
       
        getElements().add(i, nLine);
        //connect neighbors
        //add right and left to the neighbors
        if(!s.getNextTo().contains(stations.get(stations.indexOf(s)-1))){
        s.addNextTo(stations.get(stations.indexOf(s)-1));}
        if(!s.getNextTo().contains(stations.get(stations.indexOf(s)+1))){
        s.addNextTo(stations.get(stations.indexOf(s)+1));}
        //add s to right
        if(!stations.get(stations.indexOf(s)+1).getNextTo().contains(s)){
        stations.get(stations.indexOf(s)+1).addNextTo(s);}
        //add s to left
        if(!stations.get(stations.indexOf(s)-1).getNextTo().contains(s)){
        stations.get(stations.indexOf(s)-1).addNextTo(s);}

        
    }
    public void setStations(ArrayList<Station> initStations){
        stations = initStations;
    }
    public void removeStation(Station s){
        if(stations.contains(s)){
            stations.remove(s);
        }
        LineTo nline = new LineTo();
        nline.setX(line.getX());
        nline.setY(line.getY());
        nline.xProperty().bind(c.centerXProperty());
        nline.yProperty().bind(c.centerYProperty());
        int pos = getElements().indexOf(line);
        for(PathElement p: getElements()){
            if(!(p instanceof MoveTo)){
                getElements().remove(p);}
        }
        stations.remove(s);
        //line = nline;
       // getElements().add(line);
       line = nline;
        //line = nline;
        getElements().add(line);
       
        for(Station station: stations){
            addStation(s);
        }
        
    }
}
