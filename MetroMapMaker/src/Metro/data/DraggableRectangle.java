package Metro.data;

import Metro.data.Draggable;
import Metro.data.MetroState;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;

/**
 * This is a draggable rectangle for our goLogoLo application.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableRectangle extends Rectangle implements Draggable {
    double startX;
    double startY;
    Text text;
    boolean isImage;
    boolean isText = false;
    Image img;
    
    public DraggableRectangle() {
	setX(0.0);
	setY(0.0);
	setWidth(0.0);
	setHeight(0.0);
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
        isImage = false;

    }
     public DraggableRectangle(Image img) {
        setX(0.0);
	setY(0.0);
	setWidth(img.getWidth());
	setHeight(img.getHeight());
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
        setFill(new ImagePattern(img));
        isImage = true;
     }
     public DraggableRectangle(Text text) {
        this.text = text;
        //this.img = setTextImage(text);
        setX(0.0);
	setY(0.0);
	setWidth(img.getWidth());
	setHeight(img.getHeight());
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
        setFill(new ImagePattern(img));
        isImage = true;
        isText = true;
     }
 
//    @Override
//    public golState getStartingState() {
//	return golState.STARTING_RECTANGLE;
//    }
//       public Image setTextImage(Text text){
//        WritableImage snapshot = text.snapshot(new SnapshotParameters(), null);
//        ImageView image = new ImageView(snapshot);
//        this.img = image.getImage();
//        setWidth(img.getWidth());
//	setHeight(img.getHeight());
//        return img;
//    }
//    public void updateText(){
//        if(isText){
//        Stage enterTextStage = new Stage();
//        GridPane root = new GridPane();
//        Button ok = new Button("ok");
//        Button cancel = new Button("Cancel");
//        TextField tf = new TextField();
//        tf.setText(text.getText());
//        
//  
//        
//
//        HBox hboxText = new HBox(new Text("Enter Text"));
//        hboxText.setAlignment(Pos.BASELINE_RIGHT);
//        hboxText.setPadding(new Insets(15));
//        
//        hboxText.setPrefSize(500, 50);
//  
//        
//        ok.setOnMouseClicked(e -> {
//        String textString = tf.getText();
//        text = new Text(textString);
//        setTextImage(text);
//        enterTextStage.close();
//           
//        });
//        cancel.setOnMouseClicked(e -> {
//            
//            enterTextStage.close();
//
//        });
//           
//        HBox hbox = new HBox(ok, cancel);
//        hbox.setAlignment(Pos.CENTER);
//        hbox.setPadding(new Insets(15));
//        hbox.setPrefSize(100, 100);
//
//        root.add(hboxText,0,0);
//       // root.add(hboxText,0,1);
//        root.add(tf, 0, 1);
//        root.add(hbox, 0,2);
//        enterTextStage.setScene(new Scene(root));
//
//        enterTextStage.show();
//        }
//        
//    }
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
	setX(x);
	setY(y);
    }
    
    @Override
    public void drag(int x, int y) {
        if(getX() == startX){
            startX = x;
            startY = y;}
        
	double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
        
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getX();
	widthProperty().set(width);
	double height = y - getY();
	heightProperty().set(height);	
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
	widthProperty().set(initWidth);
	heightProperty().set(initHeight);
    }
    
    @Override
    public String getShapeType() {
	return RECTANGLE;
    }
    public boolean getIsImage(){
        return isImage;
    }
    public boolean getIsText(){
        return isText;
    }

    Image getImage() {
        return img;
    }
    public DraggableRectangle clone(){
       
        DraggableRectangle rectangle = new DraggableRectangle();
        rectangle.setHeight(getHeight());
        rectangle.setWidth(getWidth());
        rectangle.setFill(getFill());
        return rectangle;
    }
    public DraggableRectangle clone(Image img){
        DraggableRectangle rectangle = new DraggableRectangle(img);
        return rectangle;
    }
    public DraggableRectangle clone(Text text){
         Text newText = new Text(text.getText());
         DraggableRectangle rectangle = new DraggableRectangle(newText);
        return rectangle;
    }
    public Text getText(){
        return text;
    }

 
}   
