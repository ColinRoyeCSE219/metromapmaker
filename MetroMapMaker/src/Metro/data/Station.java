/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.data;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 *
 * @author Colin
 */
public class Station {//extends MoveTo{
    DraggableCircle c;
    Label l;
    String name;
    int position;
    int rPosition;
    private int rProperty;
    private ArrayList<Station> nextTo;
    private boolean visited;
    private ArrayList<Station> path;
    //LineTo line;
    public Station(int x, int y, String name){
      //super(x,y);
      c = new DraggableCircle(x,y, false);
      l = new Label(name);
      l.layoutXProperty().bind(c.centerXProperty());
      l.layoutYProperty().bind(c.centerYProperty());
      c.setStation(this);
      this.name = name;
      position = 1;
      nextTo = new ArrayList<Station>();
     // c.centerXProperty().bind(this.xProperty());
     // c.centerYProperty().bind(this.yProperty());
  }
    public DraggableCircle getCircle(){
        return c;
    }
    public Label getLabel(){
        return l;
    }
    public void setLabel(Label l){
      l = new Label(name);
      l.layoutXProperty().bind(c.centerXProperty());
      l.layoutYProperty().bind(c.centerYProperty());
    }
    public String getName(){
                return name;
    }

    public int getLabelPosition() {
        return position;
    }
    

    public void move(int position) {
       
        if(position%4 == 1){
       l.layoutXProperty().bind(c.centerXProperty());
        l.layoutYProperty().bind(c.centerYProperty().add(-20));
        }
        if(position%4 == 2){
        l.layoutXProperty().bind(c.centerXProperty().add(20));
        l.layoutYProperty().bind(c.centerYProperty());  
        }
        if(position%4 == 3){
        l.layoutXProperty().bind(c.centerXProperty());
        l.layoutYProperty().bind(c.centerYProperty().add(20));
        }
         if(position%4 == 0){
        l.layoutXProperty().bind(c.centerXProperty().add(-20));
        l.layoutYProperty().bind(c.centerYProperty());
        }
        this.position++;
    }
    public void rotate(){
       // if(position%4 == 1){
            l.rotateProperty().unbind();
            l.setRotate(90+rProperty);
            rProperty = rProperty+90;
//        }
//        if(position%4 == 2){
//        l.layoutXProperty().bind(c.centerXProperty().add(20));
//        l.layoutYProperty().bind(c.centerYProperty());  
//        }
//        if(position%4 == 3){
//        l.layoutXProperty().bind(c.centerXProperty());
//        l.layoutYProperty().bind(c.centerYProperty().add(20));
//        }
//         if(position%4 == 0){
//        l.layoutXProperty().bind(c.centerXProperty().add(-20));
//        l.layoutYProperty().bind(c.centerYProperty());
//        }
//        this.rPosition++;
//    }
    }
    public void addNextTo(Station s){
        nextTo.add(s);
    }
    public void removeNextTo(Station s){
        nextTo.remove(s);
    }
    public ArrayList<Station> getNextTo(){
        return nextTo;
    }

    public void setVisited(boolean b) {
        visited = b;
    }
    public boolean getVisited(){
        return visited;
    }
    public void addToPath(ArrayList<Station> list, Station s){
         list.add(s);
         path = list;
    }
    public ArrayList<Station> getPath(){
        return path;
    }
    
}
