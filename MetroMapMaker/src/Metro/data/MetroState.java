package Metro.data;

/**
 * This enum has the various possible states of the logo maker app
 * during the editing process which helps us determine which controls
 * are usable or not and what specific user actions should affect.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public enum MetroState {
    ADD_LINE,
    REMOVE_LINE,
    EDIT_LINE,
    MOVE_LINE_END,
    ADD_STATION_TO_LINE,
    ADD_NEW_STATION,
    REMOVE_STATION_FROM_LINE,
    REMOVE_STATION,
    MOVE_STATION, 
    ADD_STATION,
    SELECT, 
    DRAGGING_NOTHING, DRAGGING_STATION,
  
}
