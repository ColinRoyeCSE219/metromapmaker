package Metro.data;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.MoveTo;

/**
 * This is a draggable ellipse for our goLogoLo application.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableCircle extends Circle implements Draggable {
    double startX;
    double startY;
    Station s;
    private boolean isEnd;
    public DraggableCircle(int x, int y, boolean isEnd){
        startX = x;
	startY = y;
       if(isEnd){
       this.setRadius(7);
	setOpacity(1.0);
       start(x,y);
       this.isEnd = isEnd;
       this.setFill(Color.BLACK);
       }
       else{
	this.setRadius(20);
	setOpacity(1.0);
       start(x,y);
       this.setFill(Color.BLUE);
       }
       
    }

    public DraggableCircle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public boolean getIsEnd(){
        return isEnd;
    }
 
    
    @Override
    public void start(int x, int y) {
 
	startX = x;
	startY = y;
	 this.setCenterX(startX);
        this.setCenterY(startY);

    }
    
    

    @Override
    public void drag(int x, int y) {
    if(getX() == x){
            startX = x;
            startY = y;}
        
	double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	centerXProperty().set(newX);
	centerYProperty().set(newY);
	startX = x;
	startY = y;    }

    @Override
    public void size(int x, int y) {
        double width = x - getX();
 	double height = y - getY();
    }

    @Override
    public double getX() {
        return startX;
    }

    @Override
    public double getY() {
        return startY;
    }

    @Override
    public double getWidth() {
        return this.getRadius()*2;
    }

    @Override
    public double getHeight() {
        return this.getRadius()*2;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        startX = initX;
        startY = initY;
        this.setRadius(((int)initWidth) / 2);
    }

    @Override
    public String getShapeType() {
       return "circle"; //To change body of generated methods, choose Tools | Templates.
    }
    public void setStation(Station s){
        this.s = s;
    }
    public Station getStation(){
        return s;
    }
    public void setX(int num){
        startX=num;
    }
     public void setY(int num){
        startY=num;
    }
    
}
