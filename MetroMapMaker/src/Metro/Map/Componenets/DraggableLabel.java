/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Map.Componenets;

import Metro.data.MetroState;
import javafx.scene.control.Label;

/**
 *
 * @author Colin
 */
public class DraggableLabel extends Label implements Draggable {
    double startCenterX;
    double startCenterY;
    Label label;
    
    public DraggableLabel() {
	//setCenterX(0.0);
	//setCenterY(0.0);
	//this.setRadius(0.0);
	setOpacity(1.0);
	startCenterX = 0.0;
	startCenterY = 0.0;
        
    }
    
    @Override
    public MetroState getStartingState() {
	return MetroState.ADD_STATION;
    }
    
    @Override
    public void start(int x, int y) {
	startCenterX = x;
	startCenterY = y;
    }
    
    @Override
    public void drag(int x, int y) {
	
	double newX = 3;//getWidth() + diffX;
	double newY = 3;//getCenterY() + diffY;
	//setCenterX(newX);
	//setCenterY(newY);
	startCenterX = x;
	startCenterY = y;
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	//setCenterX(centerX);
	//setCenterY(centerY);
	//setRadius(width / 2);
	;	
	
    }
        
    @Override
    public double getX() {
	return startCenterX;//getCenterX() //- getRadiusX();
    }

    @Override
    public double getY() {
	return startCenterY;
    }

   

        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
//	setCenterX(initX + (initWidth/2));
//	setCenterY(initY + (initHeight/2));
//	setRadius(initWidth/2);
    }
    
    @Override
    public String getShapeType() {
	return "STATION";
    }
    
    public DraggableStation clone(){
        DraggableStation shape = new DraggableStation();
        shape.setLocationAndSize(startCenterX, startCenterY, getWidth(), getHeight());
        return shape;
    }    
}
