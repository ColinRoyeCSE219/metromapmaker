package Metro.Map.Componenets;

import Metro.Map.Componenets.Draggable;
import Metro.data.MetroState;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;

/**
 * This is a draggable ellipse for our goLogoLo application.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableStation extends Circle implements Draggable {
    double X;
    double Y;
    
    public DraggableStation() {
	setCenterX(0.0);
	setCenterY(0.0);
	this.setRadius(1.0);
	setOpacity(1.0);
	X = 0.0;
	Y = 0.0;
        
    }
    
    
    @Override
    public MetroState getStartingState() {
	return MetroState.ADD_STATION;
    }
    
    @Override
    public void start(int x, int y) {
	X = x;
	Y = y;
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x - X;
	double diffY = y - Y;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	X = x;
	Y = y;
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - X;
	double height = y - Y;
	double centerX = X + (width / 2);
	double centerY = Y + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadius(width / 2);
	;	
	
    }
        
    @Override
    public double getX() {
	return X;//getCenterX() //- getRadiusX();
    }

    @Override
    public double getY() {
	return Y;
    }

    @Override
    public double getWidth() {
	return getRadius() * 2;
    }

    @Override
    public double getHeight() {
	return getRadius() * 2;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadius(initWidth/2);
    }
    
    @Override
    public String getShapeType() {
	return "STATION";
    }
    
    public DraggableStation clone(){
        DraggableStation shape = new DraggableStation();
        shape.setLocationAndSize(X, Y, getWidth(), getHeight());
        return shape;
    }
}
