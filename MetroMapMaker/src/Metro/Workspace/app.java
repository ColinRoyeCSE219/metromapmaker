/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Workspace;

import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.util.logging.Level;
import java.util.logging.Logger;


import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;


public class app extends Application {

    private VBox vbox;
    @Override
    public void start(Stage stage) throws Exception {
         
        vbox = new VBox();
        Parent root = null;
         try {
             root = FXMLLoader.load(getClass().getResource("WelcomeScreen.fxml"));
         } catch (IOException ex) {
             Logger.getLogger(app.class.getName()).log(Level.SEVERE, null, ex);
         }
        if(root != null){
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        }
        else{
            System.out.print("root is null");
        }
       File[] files = new File(PATH_WORK).listFiles();
        initRecentFileButtons(files);
        Stage ns = new Stage();
        Scene nscene = new Scene(vbox);
        ns.setScene(nscene);
       // ns.show();
    
    }
        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    
    
    //fix this by making the combo box work
    
    /**
     * This method is used to select the monitor the application will open on
     * @param files
     */
    protected void initRecentFileButtons(File[] files) {
        int index = 0;
        for (File file : files) {
        if (file.isDirectory()) {
            initRecentFileButtons(file.listFiles()); // Calls same method again.
        } else {
            Button btn = new Button(file.getName());
            btn.setPrefSize(200, 200);
            vbox.getChildren().add(btn);
            
            btn.setOnAction((e) -> {
               // loadFile(new File(file.getName()), primaryStage);
            });
            System.out.println("File: " + file.getName());
        }
            }   
        }
    
}