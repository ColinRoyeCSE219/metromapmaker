/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Transactions;
import Metro.data.DraggableCircle;
import Metro.data.DraggableLabel;
import Metro.data.DraggableRectangle;
import Metro.data.Line;
import Metro.data.MetroData;
import djf.AppTemplate;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author Colin
 */
public class RemoveShape implements jTPS_Transaction{
    Node selectedShape;
    MetroData data;
    
    public RemoveShape(Node shape, MetroData data) {
        this.selectedShape = shape;
        this.data = data;
        
        //, a text, a line
    }

    @Override
    public void doTransaction() {
	//check if is image
        if(selectedShape instanceof DraggableRectangle){
            //checks if is label
            if(selectedShape instanceof DraggableLabel){
               DraggableLabel l = (DraggableLabel)selectedShape;
               data.getShapes().remove(l);
               data.getShapes().remove(l.getL());
            //checks if is image    
            }else{
           DraggableRectangle r = (DraggableRectangle)selectedShape;
           data.getShapes().remove(selectedShape);}
        
        //check if is DraggableCircle
        }else if(selectedShape instanceof DraggableCircle){
            DraggableCircle c = (DraggableCircle)selectedShape;
            //check if is station opposed to line end
              data.getShapes().remove(((DraggableCircle)data.getSelectedShape()).getStation().getLabel());

            if(!c.getIsEnd()){
                //remove from pane
                data.getShapes().remove(c);
                //remove from lists
                data.getStationsHash().remove(c.getStation());
                data.getStations().remove(c.getStation());
                
            }
         //checks if is line
        }else if(selectedShape instanceof Line){
           Line line = (Line)selectedShape;
           data.getShapes().remove(line);
           data.getShapes().remove(line.getC());
           data.getShapes().remove(line.getC2());
           data.getShapes().remove(line.getLabel());
            
           data.getLinesHash().remove(line.getName(), line);
           data.getLines().remove(line);           

        }
        
        //check if is 
    }

    @Override
    public void undoTransaction() {
        jTPS_Transaction transaction = new AddShape(selectedShape,data);
        transaction.doTransaction();
        //data.getShapes().add(selectedShape);
    }
    
}