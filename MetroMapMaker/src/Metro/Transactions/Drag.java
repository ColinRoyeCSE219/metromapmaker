/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Transactions;

import Metro.data.Draggable;
import Metro.data.DraggableCircle;
import Metro.data.DraggableRectangle;
import Metro.data.MetroData;
import Metro.data.MetroState;
import djf.AppTemplate;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author Colin
 */
public class Drag  implements jTPS_Transaction{
    Draggable selectedShape;
    AppTemplate app;
    MetroData data ;
    int x;
    int y;
    int originalX;
    int originalY;
    
    public Drag(Node shape, MetroData mData, int xLocation, int yLocation) {
        this.data = mData;
        if(shape instanceof Draggable){
        this.selectedShape = (Draggable)shape;}
        this.x = xLocation;
        this.y = yLocation;
       
        
    }

    @Override
    public void doTransaction() {
        
           // if (data.isInState(MetroState.DRAGGING_STATION)) {
            if(selectedShape instanceof DraggableCircle){
            Draggable selectedDraggableShape = (Draggable) selectedShape;
                 
               
                this.originalX = (int)selectedShape.getX();
                this.originalY = (int)selectedShape.getY();
            
            selectedDraggableShape.drag(x, y);
            }else if(selectedShape instanceof DraggableRectangle){
               Draggable selectedDraggableShape = (Draggable) selectedShape;
                selectedDraggableShape.drag(x, y);
             }
       //    }
    }

    @Override
    public void undoTransaction() {
            if(selectedShape instanceof DraggableCircle){
            Draggable selectedDraggableShape = (Draggable) selectedShape;
            selectedDraggableShape.drag(originalX, originalY);
            }else if(selectedShape instanceof DraggableRectangle){
               Draggable selectedDraggableShape = (Draggable) selectedShape;
                selectedDraggableShape.drag(originalX, originalY);
             }
           
    }
    
}
