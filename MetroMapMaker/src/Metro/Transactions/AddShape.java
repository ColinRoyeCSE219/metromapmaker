/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Transactions;

import Metro.data.DraggableCircle;
import Metro.data.DraggableLabel;
import Metro.data.DraggableRectangle;
import Metro.data.Line;
import Metro.data.MetroData;
import Metro.data.Station;
import djf.AppTemplate;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import jtps.jTPS_Transaction;

/**
 *
 * @author Colin
 */
public class AddShape implements jTPS_Transaction{
    Node shapeToAdd;
    Station stationToAdd;
    MetroData data;
    String name;
    public AddShape(Node shape, MetroData mData) {
        this.data = mData;
        this.shapeToAdd = shape;
    }
    public AddShape(Station station, String name ,MetroData mData){
        this.name = name;
        this.stationToAdd = station;
        this.data = mData;
    }

    public AddShape(Line line, String name, MetroData data) {
        this.shapeToAdd = line;
        this.name = name;
        this.data = data;
    }


    @Override
    public void doTransaction() {
        if(stationToAdd==null){
        if(shapeToAdd != null){
        //check if is circle
        if(shapeToAdd instanceof DraggableCircle){
           DraggableCircle c = (DraggableCircle)shapeToAdd;
           if(!c.getIsEnd()){
            c.getStation().setLabel(new Label(c.getStation().getName()));
            
            Label l = c.getStation().getLabel();

           //((MetroData) app.getDataComponent()).addShape(p);
           data.getShapes().add(c);
           
           data.getShapes().add(l);
           data.getStationsHash().put(c.getStation().getName(), c.getStation());
           data.getStations().add(c.getStation());
           }
        }
        //check if is image
        if(shapeToAdd instanceof DraggableRectangle && !(shapeToAdd instanceof DraggableLabel)){
        data.getShapes().add(shapeToAdd);
        //check if is label
        }else if(shapeToAdd instanceof DraggableLabel){
        DraggableLabel l = (DraggableLabel)shapeToAdd;
        data.getShapes().add(l);
        data.getShapes().add(l.getL());
        l.setOpacity(.1);

     
        //Check if is line
        }else if(shapeToAdd instanceof Line){
           Line line = (Line) shapeToAdd;
           //addShape(line);
           ObservableList<Node> shapes = data.getShapes();
           shapes.add(line);
           shapes.add(line.getC());
           shapes.add(line.getC2());
           shapes.add(line.getLabel());
           
           data.getLinesHash().put(name, line);
           data.getLines().add(line);
        }}}
        else{
         //draw station
          DraggableCircle c = stationToAdd.getCircle();
           Label l = stationToAdd.getLabel();//c.getStation().getLabel();
           if(l == null){
               c.getStation().setLabel(new Label(c.getStation().getName()));
           }
         
           //((MetroData) app.getDataComponent()).addShape(p);
           data.getShapes().add(c);
           data.getShapes().add(l);
           data.getStationsHash().put(name, stationToAdd);
           data.getStations().add(stationToAdd);
           
//           DraggableCircle c = stationToAdd.getCircle();
//           Label l = stationToAdd.getLabel();
//           //((MetroData) app.getDataComponent()).addShape(p);
//           data.getShapes().add(c);
//           data.getShapes().add(l);
//           data.getStationsHash().put(name, stationToAdd);
//           data.getStations().add(stationToAdd);
           
           
           
           
        }
       
        //ch
        
    }
        
    
    @Override
    public void undoTransaction() {
        if(stationToAdd==null && shapeToAdd != null){
            jTPS_Transaction transaction = new RemoveShape(shapeToAdd,data);
            transaction.doTransaction();
        }
//        if(shapeToAdd != null){
//        //check if is image
//        if(shapeToAdd instanceof DraggableRectangle){
//        data.getShapes().remove(shapeToAdd);
//        
//        //check if is label
//        }else if(shapeToAdd instanceof DraggableLabel){
//        DraggableLabel l = (DraggableLabel)shapeToAdd;
//        data.getShapes().remove(l.getL());
//        data.getShapes().remove(l);
//       
//     
//        //Check if is line
//        }else if(shapeToAdd instanceof Line){
//           Line line = (Line) shapeToAdd;
//           //addShape(line);
//           data.getShapes().remove(line);
//           data.getShapes().remove(line.getC());
//           data.getShapes().remove(line.getC2());
//
//           data.getShapes().remove(line.getLabel());
//           data.getLinesHash().remove(name, line);
//           data.getLines().remove(line);
//        }
//        }}
        else{
         //draw station
          DraggableCircle c = stationToAdd.getCircle();
           Label l = stationToAdd.getLabel();
           //((MetroData) app.getDataComponent()).addShape(p);
           data.getShapes().remove(c);
           data.getShapes().remove(l);
           data.getStationsHash().remove(name, stationToAdd);
           data.getStations().remove(stationToAdd);
        }
    }
    
}
