/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metro.Transactions;

import Metro.data.DraggableCircle;
import Metro.data.Line;
import Metro.data.MetroData;
import Metro.data.Station;
import djf.AppTemplate;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author Colin
 */
public class ConnectLine  implements jTPS_Transaction{
    Node selectedShape;
    AppTemplate app;
    MetroData data;
    Line line;
    Station station;
    
   
    public ConnectLine(Line l, Station s,  MetroData mData) {
        this.line = l;
        this.station = s;
        this.data = mData;
    }

    @Override
    public void doTransaction() {
        line.addStation(station);
    }

    @Override
    public void undoTransaction() {
        line.removeStation(station);
    }
    
}
