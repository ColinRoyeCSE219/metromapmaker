package Metro.file;

//import java.io.FileInputStream;

import Metro.data.DraggableCircle;
import Metro.data.Line;
import Metro.data.MetroData;
import Metro.data.Station;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.PrintWriter;
//import java.io.StringWriter;
//import java.util.HashMap;
//import java.util.Map;
//import javafx.collections.ObservableList;
//import javafx.scene.Node;
//import javafx.scene.paint.Color;
//import javafx.scene.shape.Shape;
//import javax.json.Json;
//import javax.json.JsonArray;
//import javax.json.JsonArrayBuilder;
//import javax.json.JsonNumber;
//import javax.json.JsonObject;
//import javax.json.JsonReader;
//import javax.json.JsonValue;
//import javax.json.JsonWriter;
//import javax.json.JsonWriterFactory;
//import javax.json.stream.JsonGenerator;
//import djf.components.AppDataComponent;
//import djf.components.AppFileComponent;
//import gol.data.golData;
//import gol.data.DraggableEllipse;
//import gol.data.DraggableRectangle;
//import gol.data.Draggable;
//import static gol.data.Draggable.RECTANGLE;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class MetroFiles implements AppFileComponent {
      // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_SHAPES = "shapes";
    static final String JSON_SHAPE = "shape";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_FILL_COLOR = "fill_color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    private String NAME = "name";

   private String STATIONS = "Stations";

    private String JSON_MOVETO = "MoveTo";
    private String JSON_C = "c";
    private String JSON_C2 = "c2";
    private String IS_END = "isEnd";
    private String STATION = "Station";
    private String RED = "red";
    private String BLUE = "blue";
    private String GREEN = "green";
    private String JSON_NAME = "name";
    private String JSON_RADIUS = "radius";
    private String JSON_COLOR = "color";

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	//Save Nodes on Canvas
	MetroData mData = (MetroData)data;
	//ObservableList<Node> shapes = mData.getShapes();
	// FIRST THE BACKGROUND COLOR
	//JsonObject bgColorJson = makeJsonColorObject(bgColor);

	// NOW BUILD THE JSON OBJCTS TO SAVE
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	ObservableList<Node> shapes = mData.getShapes();
	for (Node node : shapes) {
	    Node shape = node;
	    //Draggable draggableShape = ((Draggable)shape);
            if(shape instanceof DraggableCircle ){
            DraggableCircle c = (DraggableCircle)shape;
            if(!c.getIsEnd()){
            JsonObject circle = makeJsonCircleObject(c);
	    JsonObject shapeJson = Json.createObjectBuilder()
                    .add(JSON_SHAPE, circle).build();//.build(); ?
                    arrayBuilder.add(shapeJson);
                }
            }
            if(shape instanceof Line){
            JsonObject lineJson = makeJsonLineObject((Line)shape);
            JsonObject shapeJson = Json.createObjectBuilder()
                    .add(JSON_SHAPE, lineJson).build();
                    arrayBuilder.add(shapeJson);   
            }
        }
	
	JsonArray shapesArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_SHAPES, shapesArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    
        }

    private JsonObject makeJsonStationObject(Station s) {
       String name = s.getName();
       double x = s.getCircle().getX();
       double y = s.getCircle().getY();
       DraggableCircle c = s.getCircle();
       
           
//            if(!c.getIsEnd()){
//            JsonObject circle = makeJsonCircleObject(c);
//	    JsonObject shapeJson = Json.createObjectBuilder()
//                    .add(JSON_SHAPE, circle).build();//.build(); ?
//                    arrayBuilder.add(shapeJson);
//                }
       
       
       
       
        JsonObject station = Json.createObjectBuilder()
		   .add(JSON_NAME, name)
		    .add(JSON_X, x)
		    .add(JSON_Y, y).build();
               
        return station;
        
    }
    

    private JsonObject makeJsonMoveToObject(MoveTo begin) {
        //DraggableCircle draggableShape = (DraggableCircle)shape;
            String type = "MoveTo";
	    double x = begin.getX();
	    double y = begin.getY();
            //JsonObject station = makeJsonStationObject(draggableShape.getStation()); 
             // JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());
	    //JsonObject outlineColorJson = makeJsonColorObject((Color)shape.getStroke());
	    //double outlineThickness = shape.getStrokeWidth();
            
            JsonObject beingJson = Json.createObjectBuilder()
		    .add(JSON_TYPE, type)
		    .add(JSON_X, x)
		    .add(JSON_Y, y).build();
                    //.add(STATION, station)
            
            return beingJson;
    }

    private JsonObject makeJsonCircleObject(DraggableCircle shape) {
           
        DraggableCircle draggableShape = (DraggableCircle)shape;
            String type = "DraggableCircle";
	    double x = draggableShape.getX();
	    double y = draggableShape.getY();
            boolean isEnd = shape.getIsEnd();
            int radius = (int) shape.getRadius();//jsonShape.getInt(JSON_RADIUS);
            String color = shape.getFill().toString();
            //JsonObject station = makeJsonStationObject(draggableShape.getStation()); 
             // JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());
	    //JsonObject outlineColorJson = makeJsonColorObject((Color)shape.getStroke());
	    //double outlineThickness = shape.getStrokeWidth();
            JsonObject circle;
            if(!isEnd){
            String name = shape.getStation().getName();

             circle = Json.createObjectBuilder()
                    .add(JSON_RADIUS, radius)
                    .add(JSON_COLOR, color)
                    .add(NAME, name)
		    .add(JSON_TYPE, type)
		    .add(JSON_X, x)
		    .add(JSON_Y, y)
                    //.add(STATION, station)
		    .add(IS_END, isEnd).build();
            }
            else{
                    circle = Json.createObjectBuilder()
		    .add(JSON_TYPE, type)
		    .add(JSON_X, x)
		    .add(JSON_Y, y)
                    //.add(STATION, station)
		    .add(IS_END, isEnd).build();
            }
            return circle;
    }
    
//    private JsonObject makeJsonColorObject(Color color) {
////	JsonObject colorJson = Json.createObjectBuilder()
////		.add(JSON_RED, color.getRed())
////		.add(JSON_GREEN, color.getGreen())
////		.add(JSON_BLUE, color.getBlue())
////		.add(JSON_ALPHA, color.getOpacity()).build();
////	return colorJson;
//return null;
//    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        MetroData dataManager = (MetroData)data;
	//dataManager.resetData();
	
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
	// LOAD THE BACKGROUND COLOR
	//Color bgColor = loadColor(json, JSON_BG_COLOR);
	//dataManager.setBackgroundColor(bgColor);
	
	// AND NOW LOAD ALL THE SHAPES
	JsonArray jsonShapeArray = json.getJsonArray(JSON_SHAPES);
	for (int i = 0; i < jsonShapeArray.size(); i++) {
	    JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
	    Shape shape = loadShape(jsonShape, dataManager);
            if(shape instanceof DraggableCircle){
                DraggableCircle c = (DraggableCircle)shape;
                dataManager.addShape(shape);
                dataManager.addShape(c.getStation().getLabel());

                
            }
            if(shape instanceof Line){
                
                Line line = (Line)shape; 
                dataManager.addShape(line);
            	dataManager.addShape(line.getC());
                dataManager.addShape(line.getC2());
                dataManager.addShape(line.getLabel());
            }
	}
        //first load in all the lines and stations
        //then connect them
        ObservableList<Node> shapes = ((MetroData)data).getShapes();
        
        for (int i = 0; i < shapes.size(); i++) {
            if(shapes.get(i) instanceof Line){
                //connect line to station
                Line line = (Line)shapes.get(i);
                line.setStations(new ArrayList<Station>());
                JsonArray stations = line.getStationsJson();
                for(JsonValue j : stations){
                    JsonReader jsonReader = Json.createReader(new StringReader(j.toString()));
                    JsonObject obj = jsonReader.readObject();
                    jsonReader.close();
                    String key = obj.getString(JSON_NAME);
                    if(dataManager.getStationsHash().get(key) != null){
                    Station connectedStation = dataManager.getStationsHash().get(key);
                    line.addStation(connectedStation);
//                    dataManager.setSelectedShape(connectedStation.getCircle());
//                    dataManager.setCurrentLine(line);
                    }
                }
                    
                
                
            ///////
            }
        }
	
        
        
    }
      private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
          
    private Shape loadShape(JsonObject jsonShape, MetroData data) {
	// FIRST BUILD THE PROPER SHAPE TYPE
        Shape shape = null;
        jsonShape = jsonShape.getJsonObject("shape");
        String type = jsonShape.getString(JSON_TYPE);
        if(type.equals("DraggableCircle")){
            //make the shape
            //build the station it is linked to
            
            int x = jsonShape.getInt(JSON_X);
            int y = jsonShape.getInt(JSON_Y);
            int radius = jsonShape.getInt(JSON_RADIUS);
            String color = jsonShape.getString(JSON_COLOR);
            String name = jsonShape.getString(JSON_NAME);
            
            Station s = new Station(x,y,name);
            s.getCircle().setRadius(radius);
            s.getCircle().setFill(Color.web(color));
            shape = s.getCircle();
            data.getStations().add(s);
            data.getStationsHash().put(s.getName(), s);
            
            //add the shape to the obserbable list
            
            
        }    
        if(type.equals("Line")){
        String name = jsonShape.getString(JSON_NAME);
        JsonObject moveto = jsonShape.getJsonObject(JSON_MOVETO);
        JsonObject c = jsonShape.getJsonObject(JSON_C);
        JsonObject c2 = jsonShape.getJsonObject(JSON_C2);
        JsonArray jsonStations = null;
        String color = jsonShape.getString(JSON_COLOR);
        if(jsonShape.getJsonArray(STATIONS) != null){
         jsonStations = jsonShape.getJsonArray(STATIONS);
       }
        

        int x = c.getInt(JSON_X);
        int y = c.getInt(JSON_Y);
        MoveTo mIn = new MoveTo(x,y);
        x = c2.getInt(JSON_X);
        y = c2.getInt(JSON_Y);
        LineTo lIn = new LineTo(x,y);
        

        // make the stations last
//       JsonArray stationsJson = c.getJsonArray(STATIONS);
//         for (javax.json.JsonValue jsonValue : stationsJson) {
//            System.out.println(jsonValue);
//            }   
        Line line = new Line(name, mIn, lIn, jsonStations);
        data.getLinesHash().put(name, line);
        data.getLines().add(line);
        line.setStroke(Color.web(color));
        shape = line;
        
      
            //add the shape to the obserbable list
        }
        
           return  shape;
        
        
       

    

    }
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        MetroData mData = (MetroData)data;
	//ObservableList<Node> shapes = mData.getShapes();
	// FIRST THE BACKGROUND COLOR
	//JsonObject bgColorJson = makeJsonColorObject(bgColor);

	// NOW BUILD THE JSON OBJCTS TO SAVE
	ObservableList<Node> shapes = mData.getShapes();
        JsonArrayBuilder lines = Json.createArrayBuilder();
        JsonArrayBuilder stations = Json.createArrayBuilder();
       
	for (Node node : shapes) {
	    Node shape = node;
	    //Draggable draggableShape = ((Draggable)shape);
            if(shape instanceof DraggableCircle ){
//                JsonObject station = makeJsonStationObject(((DraggableCircle)shape).getStation());
//                stationsBuilder.add(station);
                //add to circle json
                if(!((DraggableCircle)shape).getIsEnd()){
                JsonObject station = makeJsonStationObject(((DraggableCircle)shape).getStation());
                stations.add(station);
                }
                
            }
            if(shape instanceof Line){
        //name
            
            Line line = ((Line)shape);
            JsonObjectBuilder lineArray = Json.createObjectBuilder();
            String name = null;
            name = line.getName();
            //JsonObject nameObj = Json.createObjectBuilder().add(JSON_NAME, name).build();
            lineArray.add("name", line.getName());
            //lines
            //isCircular
             //JsonObject isCircular = Json.createObjectBuilder().add("circular", "false").build();
                           
                            lineArray.add("circular", "false");
            
                //color
                String colorString = line.getC().getFill().toString();
                    Color color = Color.web(colorString);
                    
                    //red
                    String red = ((Double)color.getRed()).toString();
                    //green
                    String green = ((Double)color.getGreen()).toString();
                    //blue
                    String blue = ((Double)color.getBlue()).toString();
                    //alpha
                    String alpha = ((Double)color.getOpacity()).toString();
                    
                    JsonObject colorObj = Json.createObjectBuilder()
                            .add("red", red)
                            .add("green", green)
                            .add("blue", blue)
                            .add("alpha",alpha).build();
                           
                            lineArray.add("color", colorObj);

                //station names
                    JsonArrayBuilder stationNames = Json.createArrayBuilder();
                    for(Station s : line.getStations()){
                        stationNames.add(s.getName());
                    }
                    JsonArray stationNamesObj = stationNames.build();
                    JsonObject lineObj = lineArray.add("station_names", stationNamesObj).build();
                    lines.add(lineObj);
//                    lineArray.add("station_names" , stationNamesObj);
//                    JsonObject lineArray3 = lineArray.build();
//                    JsonObject station_names = Json.createObjectBuilder()
//                            .add("station_names", lineArray3).build();
                    
                    
            }
        }
            	
            JsonArray linesArray2 = lines.build();
            JsonArray stationsArray = stations.build();
            JsonObject export = Json.createObjectBuilder()
                    .add("lines",linesArray2)
                    .add("stations", stationsArray).build();
            
        
            //create json lines and circles
         
            //stations
                //name
                //x
                //y
            
            
            
                //build stations
           
//            JsonObject circle = makeJsonCircleObject((DraggableCircle)shape);
//            String type = "DraggableCircle";
//	    JsonObject shapeJson = Json.createObjectBuilder()
//		    .add(JSON_TYPE, type)
//                    .add(JSON_SHAPE, circle).build();//.build(); ?
//                arrayBuilder.add(shapeJson);
            
            
                         // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(export);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(export);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
            
                        // build ciry name

        
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
//   

    private JsonArray makeJsonStationsObject(Line line) {
        ArrayList<Station> s = line.getStations();
              JsonArrayBuilder stations = Json.createArrayBuilder();
              
            
        
        for(Station i : s){
		stations.add(makeJsonStationObject(i));
        }
        JsonArray o =  stations.build();
            
        return o;
                
                
    }

    private JsonObject makeJsonLineObject(Line line) {
            String type = "Line";
            double x = line.getLayoutX();
            double y = line.getLayoutY();
            JsonObject begin =  makeJsonMoveToObject(line.getBegin());
            JsonObject c = makeJsonCircleObject(line.getC());
            JsonObject c2 = makeJsonCircleObject(line.getC2());
            JsonArray stations = makeJsonStationsObject(line);
            String color = line.getStroke().toString();

            JsonObject lineJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, type)
                    .add(JSON_NAME, line.getName())
                    .add(JSON_MOVETO, begin)
		    .add(JSON_C, c)
                    .add(JSON_C2, c2)
                    .add(STATIONS, stations)
                    .add(JSON_COLOR, color).build();
            return lineJson;
            
            
    }
    

}